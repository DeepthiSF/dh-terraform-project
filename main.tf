terraform {
    required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
      }
    }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}

# Create a VPC
resource "aws_vpc" "dhTerraformVPC" {
  cidr_block = "10.0.0.0/16"
  tags = {
      Name = "dhTerraformVPC"
  }
}

#Public Subnet
resource "aws_subnet" "dh-public-subnet" {
  vpc_id     = aws_vpc.dhTerraformVPC.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "dh-public-subnet"
  }
}

#Private Subnet
resource "aws_subnet" "dh-private-subnet" {
  vpc_id     = aws_vpc.dhTerraformVPC.id
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "dh-private-subnet"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "dh-Terraform-IGW" {
  vpc_id = aws_vpc.dhTerraformVPC.id
  # comment
  tags = {
    Name = "dh-Terraform-IGW"
  }
}

# Route table for Public Subnet
resource "aws_route_table" "dh-Terraform-Public-Subnet-RT" {
  vpc_id = aws_vpc.dhTerraformVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dh-Terraform-IGW.id
  }

  tags = {
    Name = "dh-Terraform-Public-Subnet-RT"
  }
}

# Route table association for Public subnet
resource "aws_route_table_association" "dcTerraformSubnetAssociation" {
  subnet_id      = aws_subnet.dh-public-subnet.id
  route_table_id = aws_route_table.dh-Terraform-Public-Subnet-RT.id
}

# NAT Gateway
resource "aws_eip" "dh-public-nat-gateway" {
  vpc = true
}

resource "aws_nat_gateway" "dh-public-nat-gateway" {
  allocation_id = aws_eip.dh-public-nat-gateway.id
  subnet_id     = aws_subnet.dh-public-subnet.id

  tags = {
    Name = "dh-public-nat-gateway"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.dh-Terraform-IGW]
}

output "dh-public-nat-gateway-ip" {
  value = aws_eip.dh-public-nat-gateway.public_ip
}

# Route table for Private Subnet
resource "aws_route_table" "dh-Terraform-Private-Subnet-RT" {
  vpc_id = aws_vpc.dhTerraformVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.dh-public-nat-gateway.id
  }

  tags = {
    Name = "dh-Terraform-Private-Subnet-RT"
  }
}

# Route table association for Private subnet
resource "aws_route_table_association" "dcTerraformPrivateSubnetAssociation" {
  subnet_id      = aws_subnet.dh-private-subnet.id
  route_table_id = aws_route_table.dh-Terraform-Private-Subnet-RT.id
}

# Create Security Group to associate with the Front End EC2 instance in the Public subnet
resource "aws_security_group" "dh-Terraform-FrontEnd-WebServer-SG" {
  name        = "dh-Terraform-FrontEnd-WebServer-SG"
  description = "dh-Terraform-FrontEnd-WebServer-SG"
  vpc_id      = aws_vpc.dhTerraformVPC.id

  # Inbound traffic from 0.0.0.0/0 via HTTP
  ingress {
    description = "HTTP inbound"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound traffic from 0.0.0.0/0 via SSH
  ingress {
    description = "SSH inbound"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Front end app to the express backend server and vice versa"
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["10.0.2.0/24"]
    // security_groups = ["${aws_security_group.dh-Terraform-Express-Backend-Server-SG.id}"]
  }

  # Outbound traffic from EC2 to outside and also to the Backend server because "10.0.2.0/24" is a subset of "0.0.0.0/0"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

#   # Outbound traffic from front end app to the express backend server
#   egress {
#     description = "Front end app to the express backend server"
#     from_port   = 5000
#     to_port     = 5000
#     protocol    = "tcp"
#     cidr_blocks = ["10.0.2.0/24"]
#    // security_groups = ["${aws_security_group.dh-Terraform-Express-Backend-Server-SG.id}"]
#   }

  tags = {
    Name = "dh-Terraform-FrontEnd-WebServer-SG"
  }
}

# Create Security Group to associate with the Mongo database EC2 instance in the Private subnet
resource "aws_security_group" "dh-Terraform-Mongo-DB-SG" {
  name        = "dh-Terraform-Mongo-DB-SG"
  description = "dh-Terraform-Mongo-DB-SG"
  vpc_id      = aws_vpc.dhTerraformVPC.id

  # Inbound traffic from Backend Express Webserver to the Mongo DB
  ingress {
    description = "Database Connection"
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["10.0.2.0/24"]
    // security_groups = ["${aws_security_group.dh-Terraform-Express-Backend-Server-SG.id}"]
  }

  # Inbound traffic from public subnet via SSH
  ingress {
    description = "SSH inbound from public subnet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }

  # Outbound traffic from Mongo database to anywhere
  egress {
    description = "Mongo DB to anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    # security_groups = ["${aws_security_group.dh-Terraform-Express-Backend-Server-SG.id}"]
  }  

  tags = {
    Name = "dh-Terraform-Mongo-DB-SG"
  }
}

# # Create Security Group to associate with the Express Backend Server EC2 instance in the Private subnet
resource "aws_security_group" "dh-Terraform-Express-Backend-Server-SG" {
  name        = "dh-Terraform-Express-Backend-Server-SG"
  description = "dh-Terraform-Express-Backend-Server-SG"
  vpc_id      = aws_vpc.dhTerraformVPC.id

  # Inbound traffic from Mongo DB to Backend Express Webserver
  ingress {
    description = "Database Connection"
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
   // security_groups = ["${aws_security_group.dh-Terraform-Mongo-DB-SG.id}"]
    cidr_blocks = ["10.0.2.0/24"]
  }

  # Inbound traffic from public subnet via SSH
  ingress {
    description = "Admin SSH from public subnet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }

  # Inbound traffic from Front end app to the Express backend server
  ingress {
    description = "Front end to Express backend server"
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
    // security_groups = ["${aws_security_group.dh-Terraform-FrontEnd-WebServer-SG.id}"]
  }

  # Outbound traffic from Express Backend webserver to anywhere
  egress {
    description = "Express backend server to anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    # security_groups = ["${aws_security_group.dh-Terraform-Express-Backend-Server-SG.id}"]
  }  

#   # Outbound traffic from Mongo database to Express Backend webserver
#   egress {
#     description = "Express backend server to Front end App"
#     from_port   = 5000
#     to_port     = 5000
#     protocol    = "tcp"
#     cidr_blocks = ["10.0.1.0/24"]
#     security_groups = ["${aws_security_group.dh-Terraform-FrontEnd-WebServer-SG.id}"]
#   }

#   # Outbound traffic from Express Backend webserver to Mongo DB
#   egress {
#     description = "Express backend server to Mongo DB"
#     from_port   = 27017
#     to_port     = 27017
#     protocol    = "tcp"
#     cidr_blocks = ["10.0.2.0/24"]
#     //security_groups = "sg-07b67e1e39a9526b1"
#     security_groups = ["${aws_security_group.dh-Terraform-Mongo-DB-SG.id}"]
#   }

  tags = {
    Name = "dh-Terraform-Express-Backend-Server-SG"
  }
}

resource "aws_instance" "dh-Terraform-EC2-Web" {
  ami = "ami-002068ed284fb165b"
  instance_type = "t2.micro"
  key_name = "dh-ssh"
  security_groups = [ aws_security_group.dh-Terraform-FrontEnd-WebServer-SG.id ]

  tags = {
    Name = "DH-Web-EC2"
  }
  subnet_id = aws_subnet.dh-public-subnet.id
}

resource "aws_instance" "dh-Terraform-EC2-Backend" {
  ami = "ami-002068ed284fb165b"
  instance_type = "t2.micro"
  key_name = "dh-ssh"
  security_groups = [ aws_security_group.dh-Terraform-Express-Backend-Server-SG.id ]

  tags = {
    Name = "DH-Backend-EC2"
  }
  subnet_id = aws_subnet.dh-private-subnet.id
}

resource "aws_instance" "dh-Terraform-EC2-Mongo-DB" {
  ami = "ami-002068ed284fb165b"
  instance_type = "t2.micro"
  key_name = "dh-ssh"
  security_groups = [ aws_security_group.dh-Terraform-Mongo-DB-SG.id ]

  tags = {
    Name = "DH-Mongo-DB-EC2"
  }
  subnet_id = aws_subnet.dh-private-subnet.id
}
