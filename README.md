# DH Terraform Project


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:1ce6517c692dd7027dd3a72593ed17a0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:1ce6517c692dd7027dd3a72593ed17a0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:1ce6517c692dd7027dd3a72593ed17a0?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/DeepthiSF/dh-terraform-project.git
git branch -M main
git push -uf origin main
```


## Name
Deepthi and Hunter's "Pomotodo React App on AWS utilizing Mongo database"

## Description
The Pomotodo application has a timer and the ability to add "to do" items on the front-end. The "to do" items are persisted on a mongo database on the backend, by connecting to a private backend server to add the records to the database.

This application uses a simple network infrastructure by:

1. Utilizing a public subnet for the web server
2. A private subnet for the backend server and Mongo database
3. A NAT gateway to connect the private subnet to the public subnet
4. An Internet gateway to connect to the Internet
5. Two route tables, and route table associations, to route traffic to the Internet gateway
6. And security groups to only allow traffic inbound and outbound traffic on certain ports and from specific IP addresses. 

## Visuals

https://gitlab.com/DeepthiSF/dh-terraform-project/-/blob/main/project_flow_latest.jpg

## Installation

To install this project, clone down this GitLab project: https://gitlab.com/DeepthiSF/dh-terraform-project

Install Node.js by following the steps in: https://nodejs.org/en/download/

Install the AWS CLI by following the steps in: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

Install Terraform by following the steps in: https://learn.hashicorp.com/tutorials/terraform/install-cli

The preferred code editor is VSCode, install VSCode by following the steps in: https://code.visualstudio.com/download, and install the VSCode terraform extension by:

1. Search for "Terraform" in the VSCode Extension Marketplace. Make sure the author is Hashicorp.
2. Click on install.

## Usage
Adding a to-do item will create a record on the database using a singular ID. The ID will hold an array of any to-do items that were added. The to-do items will display on the front-end and will be persisted on the back-end in the Mongo database. 


## Support
For assistance, please reference the terraform documentation at: https://www.terraform.io/.

If additional assistance is needed, contact:

Deepthi Gaddapati at deepthi.gaddapati.f2jn@statefarm.com
Hunter Guthrie at hunter.guthrie.t7uy@statefarm.com

## Contributing
To contribute to the project, clone the project down and create a merge request. One of the maintainers (Deepthi Gadapati or Hunter Guthrie) will review the merge request and approve it when the code is reviewed and approved.

## Authors and acknowledgment
Galvanize, Inc.
Deepthi Gadapati
Hunter Guthrie

## Project status
The frontend and backend connections are in place and work with curl commands. For example:

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"hunter","desc":"description","id":"44805443-2194-42bb-ae1f-5a4fcea262e4","dateCreated":1639691704449,"dateCompleted":null,"completed":false,"tags":[],"pomodoroCount":0}' \
  http://<backend-server-private-IP>:5000/api/todo-data

However, a proxy server must be set up in main.tf for "get" requests and "post" requests to persist by simply using the front-end. 

